#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define sizeBuffer 40
#define sizeBufferNombre 200
#define sizeBufferNumeros 20
#define sizeBufferCaracteres 80
#define sizeVec 100
#define message "El nombre introducido es: %s\r\n"
#define message2 "X1 = %.1f X2 = %.1f\r\n"
#define base(x) 1 + (x/100)
#define nueva_carta 1
#define fin_juego 2
#define maxPoint 21
#define ejemplo  11


#if ejemplo == 1

typedef struct{
int i, j;
long ix;
unsigned u;
float x;
double dx;
char c;
}data_t;

data_t data;


void funcionA(){
    data.i = 102;
    data.ix = -158693157400;
    data.j = -56;
    data.x = 12.687;
    data.dx = 0.00000025;
    data.u = 35460;
    data.c = 'C';
}

void funcionB(){
    printf("El valor de i es: %4d\r\n", data.i);
    printf("El valor de j es: %4d\r\n", data.j);
    printf("El valor de x es: %14.8E\r\n", data.x);
    printf("El valor de dx es: %14.8E\r\n", data.dx);
}

void funcionC(){
    printf("El valor de i es: %5d\r\n", data.i);
    printf("El valor de j es: %5d\r\n", data.j);
    printf("El valor de u es: %5d\r\n", data.u);
    printf("El valor de ix es: %12ld\r\n", data.ix);
    printf("El valor de x es: %10.5f\r\n", data.x); 
}

void funcionD(){
    printf("El valor de i es: %5d el valor de j es: %5d el valor de u es: %5d\r\n", data.i, data.j, data.u);
    printf("\n");
    printf("El valor de ix es: %12ld el valor de x es: %10.5f\r\n", data.ix, data.x);
}

void funcionE(){
    printf("El valor de i es: %6d\n\r\n",data.i);
    printf("El valor de u es: %6d\n\r\n",data.u);
    printf("El valor de c es: %d\n\r\n",data.c);
}

void funcionF(){
    printf("El valor de j es: %5d\n\r\n",data.i);
    printf("El valor de u es: %5d\n\r\n",data.u);
    printf("El valor de x es: %11.4f\n\r\n",data.x);
}

void funcionG(){
    printf("El valor de j es: %-5d\n\r\n",data.i);
    printf("El valor de u es: %-5d\n\r\n",data.u);
    printf("El valor de x es: %-11.4f\n\r\n",data.x);
}

void funcionH(){
    printf("El valor de j es: %-5d\n\r\n",data.i);
    printf("El valor de u es: %-5d\n\r\n",data.u);
    printf("El valor de x es: %-11.4f\n\r\n",data.x);
    printf("El valor de j es: %+5d\n\r\n",data.i);
    printf("El valor de u es: %+5d\n\r\n",data.u);
    printf("El valor de x es: %+11.4f\n\r\n",data.x);
}

#elif ejemplo == 2

void pedirNombre(){
    char buffer[sizeBuffer];
    char nombre[sizeBufferNombre];
    printf("Por favor, introduce tu nombre: ");
    fgets(buffer, sizeBuffer, stdin);
    sprintf(nombre, message , buffer);
    printf("%s",nombre);
}

#elif ejemplo == 3

void mostrarNumeros(){
    char mostrar[sizeBufferNumeros];
    float x1 = 8.0, x2 = -2.5;
    sprintf(mostrar, message2, x1, x2);
    printf("%s",mostrar);
}

#elif ejemplo == 4

void pedirDatos(int *a, int *b){
    printf("Introduzca el valor de 'a': \n\r");
    scanf("%d",a);
    printf("Introduzca el valor de 'b': \n\r");
    scanf("%d",b);
}

void sumarDatos(int *a, int *b){
    *a += *b;
}

void sumar(){
    int a, b;
    pedirDatos(&a, &b);
    sumarDatos(&a, &b);
    printf("El resultado de la suma es: %d\n\r", *a);
}

#elif ejemplo == 5

void invertirCaracteres(){
    char texto[sizeBufferCaracteres];
    char c;
    do{
        printf("Introduce una cadena de texto\n\r");
        fgets(texto, sizeBufferCaracteres, stdin);
        fflush(stdin);
        if(texto[0] != '-' && texto[1] != '1'){
            uint8_t len = strlen(texto);
            for(uint8_t i = 0, j = len ; i = j + 1; i++, j--){
            c = texto[i], texto[i] = texto[j], texto[j] = c ; 
            printf("%c", texto[i]);
        }
        printf("\n\r");   
        }
      } while (texto[0] != '-' && texto[1] != '1');
    printf("salio!\n\r");
}

#elif ejemplo == 6

float potenciacion(float base, uint8_t exponente){
    float resultado = 0;
    if(exponente == 0){
        resultado = 1.0;
    }else if (exponente > 0){
        resultado = base * potenciacion(base, exponente - 1);
    }
    return resultado;
}

void dineroAcumulador(){
    float F, A, i;
    int n;
    printf("Ingrese dinero en cuenta de ahorro\n\r");
    scanf("%f",&A);
    printf("Ingrese cantidad de años\n\r");
    scanf("%d",&n);
    printf("Ingrese interes anual\n\r");
    scanf("%f",&i);
    printf("\n");
    printf("Dinero\t\t\tAño\n\r");
    printf("------------------------------\n\r");
    for(uint8_t j = 0; j < n; j++){
        F += A* potenciacion(base(i),j);
        printf("%.4f\t\t%d\n\r", potenciacion(base(i),j+1), j+1);
        printf("------------------------------\n\r");
    }

}

#elif ejemplo == 7

int menor_mayor_absoluto( const void* ptrA, const void* ptrB ){
    if( abs(*(float *)ptrA)  == abs(*(float *)ptrB) ){
        return 0;
    }
    else if ( abs(*(float *)ptrA)  >= abs(*(float *)ptrB) )
    {
        return 1;
    }else return -1;  
}

int menor_mayor_algebraico( const void* ptrA, const void* ptrB ){
    if( *(float *)ptrA  == *(float *)ptrB ){
        return 0;
    }
    else if ( *(float *)ptrA  >= *(float *)ptrB )
    {
        return 1;
    }else return -1;  
}

int mayor_menor_absoluto( const void* ptrA, const void* ptrB ){
    if( abs(*(float *)ptrA)  == abs(*(float *)ptrB) ){
    return 0;
    }
    else if ( abs(*(float *)ptrA)  >= abs(*(float *)ptrB) )
    {
        return -1;
    }else return 1;  
}

int mayor_menor_algebraico( const void* ptrA, const void* ptrB ){
    if( *(float *)ptrA  == *(float *)ptrB ){
        return 0;
    }
    else if ( *(float *)ptrA  >= *(float *)ptrB )
    {
        return -1;
    }else return 1;  
}

int (*criterio[])(const void*,const void*) = {menor_mayor_absoluto, menor_mayor_algebraico, mayor_menor_absoluto, mayor_menor_algebraico};

void ordenamientoNumeros(){
    int opcion;
    float vec[] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
    uint8_t talla = sizeof(vec[0]);
    uint8_t longuitud = sizeof(vec)/talla;
    while(1){
        do{
        printf("Indique el tipo de ordenamiento: \n\r");
        printf("\n 1. Ordenamiento menor a mayor en valor absoluto");
        printf("\n 2. Ordenamiento menor a mayor en valor algebraico");
        printf("\n 3. Ordenamiento mayor a menor en valor absoluto");
        printf("\n 4. Ordenamiento mayor a menor en valor algebraico");
        printf("\n");
        scanf("%d",&opcion);
        }while ( opcion!= 1 && opcion!= 2 && opcion!= 3 && opcion!= 4);
        qsort(vec, longuitud, talla, criterio[opcion-1]);
        for(uint8_t i = 0; i < longuitud; i++){
            printf("%.2f ",vec[i]);
        }
        printf("\n\n");
    }
}

#elif ejemplo == 8

typedef struct{
    char  nombre[sizeBufferNombre];
    float nota_media;
}alumno_t;

void notasProcesador(){
    int nalumnos, nparciales, max = 0, index;
    float acum, acum_general, media_general;
    printf("Indique la cantidad de alumnos: \n");
    scanf("%d",&nalumnos);
    alumno_t alumnos[nalumnos];
    for (uint8_t i = 0; i < nalumnos; i++)
    {
        printf("Indique el nombre del alumno #%d: \n", i+1);
        scanf("%s", alumnos[i].nombre);
        printf("Indique la cantidad de parciales del alumno %s: \n", alumnos[i].nombre);
        scanf("%d",&nparciales);
        float calificaciones[nparciales];
        for (uint8_t j = 0; j < nparciales; j++)
        {
            printf("Indique la nota del parcial #%d del alumno %s: \n", j+1, alumnos[i].nombre);
            scanf("%f", &calificaciones[j]);
            acum += calificaciones[j];
        }
        alumnos[i].nota_media = acum/nparciales;
        if(alumnos[i].nota_media < max){
            max = alumnos[i].nota_media;
            index = i;
        }
        acum_general = alumnos[i].nota_media;
    }
    printf("El promedio de las notas medias es de %.2f y el alumno con mejor promedio fue %s\n\r", acum_general/nalumnos, alumnos[index].nombre);
}

#elif ejemplo == 9

typedef enum{
    jugadorOne,
    jugadorTwo,
}turno_t;

typedef enum{
    Oro = 1,
    Basto,
    Copa,
    Espada
}carta_t;

typedef struct {
    char nombre[sizeBufferNombre];
    uint8_t pointAcum;
    uint8_t pointCurrent;
}Jugador_t;

Jugador_t jugadores[2];
turno_t turno;
carta_t carta;

void backJacj(){
    int opcion = 0, num = 0;
    printf("Ingrese nombre del jugador #1\n\r");
    scanf("%s", jugadores[0].nombre);
    printf("Ingrese nombre del jugador #2\n\r");
    scanf("%s", jugadores[1].nombre);
    srand(time(NULL));
    do{
    printf("\n 1. Pedir nueva carta");
    printf("\n 2. Retirarse del juego");
    printf("\n");
    scanf("%d",&opcion);
    if (opcion != nueva_carta && opcion != fin_juego){
        printf("Ingrese una opcion valida\n\r");
    }else{ 
        if (opcion == nueva_carta){
            switch (turno){
            case jugadorOne:
                printf("Turno del jugador %s\n\r",jugadores[0].nombre);
                jugadores[0].pointCurrent = (rand()%10)+1;
                carta = (rand()%4)+1;
                if (carta == Oro){
                    printf("de Oro\n\n");
                    }
                else if(carta == Basto){
                    printf("de Basto\n\n");
                    }
                else if(carta == Copa){
                    printf("de Copa\n\n");
                    }
                else if(carta == Espada){
                    printf("de Espada\n\n");
                    }
                jugadores[0].pointAcum += jugadores[0].pointCurrent; 
                if (jugadores[0].pointAcum > maxPoint){
                printf("Perdiste!!!\n\n");
                printf("Gano el jugador %s con %d puntos\n\r", jugadores[1].nombre, jugadores[1].pointAcum);
                opcion = fin_juego;
                }
                turno = jugadorTwo;
                break;
            case jugadorTwo:
                printf("Turno del jugador %s\n\r",jugadores[1].nombre);
                jugadores[1].pointCurrent = (rand()%10)+1;
                carta = (rand()%4)+1;
                if (carta == Oro){
                    printf("de Oro\n\n");
                    }
                else if(carta == Basto){
                    printf("de Basto\n\n");
                    }
                else if(carta == Copa){
                    printf("de Copa\n\n");
                    }
                else if(carta == Espada){
                    printf("de Espada\n\n");
                    }
                jugadores[1].pointAcum += jugadores[1].pointCurrent; 
                if (jugadores[1].pointAcum > maxPoint){
                printf("Perdiste!!!\n\n");
                printf("Gano el jugador %s con %d puntos\n\r", jugadores[0].nombre, jugadores[0].pointAcum);
                opcion = fin_juego;
                }
                turno = jugadorOne;
                break;
            }
    }else {
        opcion = fin_juego;
        if(jugadores[1].pointAcum > jugadores[0].pointAcum){
            printf("Gano el jugador %s con %d puntos\n\r", jugadores[1].nombre, jugadores[1].pointAcum);
            printf("El jugador %s obtuvo %d puntos\n\r", jugadores[0].nombre, jugadores[0].pointAcum);
        }
        else if (jugadores[0].pointAcum > jugadores[1].pointAcum){
            printf("Gano el jugador %s con %d puntos\n\r", jugadores[0].nombre, jugadores[0].pointAcum);
            printf("El jugador %s obtuvo %d puntos\n\r", jugadores[1].nombre, jugadores[1].pointAcum);
        }
        else printf("Empataron con %d puntos\n\r", jugadores[0].pointAcum);
    }
    }
    }while(opcion != fin_juego);
}

#elif ejemplo == 10

typedef struct {
    char *nombre;
    int victorias;
    int derrotas;
    int empates;
    int goles;
}equipos_t;

equipos_t * dinamicStruct(uint8_t gamers){
    equipos_t * retVal = (equipos_t *)malloc(sizeof(equipos_t)*gamers);
    if(retVal == NULL){
        return retVal;
    }
    retVal->nombre = (char *)malloc(sizeof(char)*sizeBufferNombre);
    if(retVal->nombre == NULL){
        free(retVal);
        return NULL;
    }
    return retVal;
}

void equiposFutbol(){
    int n = 0;
    char guiones[] = "--------------------";
    printf("Ingrese la cantidad de equipos del torneo\n\r");
    scanf("%d",&n);
    equipos_t *equipos = dinamicStruct(n);
    if(equipos == NULL){
        printf("Error!!\n\r");
        while(1);
    }
    for (uint8_t i = 0; i < n; i++){
        printf("Ingrese el nombre del equipo #%d\n\r",i+1);
        scanf("%s",(equipos+i)->nombre);
        printf("Ingrese las victorias del equipo #%d\n\r",i+1);
        scanf("%d",&(equipos+i)->victorias);
        printf("Ingrese las derrotas del equipo #%d\n\r",i+1);
        scanf("%d",&(equipos+i)->derrotas);
        printf("Ingrese los empates del equipo #%d\n\r",i+1);
        scanf("%d",&(equipos+i)->empates);
        printf("Ingrese los goles del equipo #%d\n\r",i+1);
        scanf("%d",&(equipos+i)->goles);
    }
        printf("+%s+%s+%s+%s+\n", guiones, guiones,  guiones,  guiones);
        printf("|%-20s|%-20s|%-20s|%-20s|\n", "EQUIPO", "VICTORIAS","DERROTAS","GOLES");
        printf("+%s+%s+%s+%s+\n", guiones, guiones,  guiones,  guiones);
        for(uint8_t i = 0; i < n; i++){
            printf("|%-20s|%-20d|%-20d|%-20d|\n", (equipos+i)->nombre, (equipos+i)->victorias,(equipos+i)->derrotas,(equipos+i)->goles);
            printf("+%s+%s+%s+%s+\n", guiones, guiones,  guiones,  guiones);
        }
}

#elif ejemplo == 11

#define MAXIMA_LONGITUD_PALABRA 100

typedef struct{
  char palabra[MAXIMA_LONGITUD_PALABRA];
  int frecuencia;
}DetalleDePalabra;

typedef struct{
  DetalleDePalabra detalleDePalabra;
  struct nodo *siguiente;
}nodo;

nodo *superior = NULL;

void agregar(DetalleDePalabra detalleDePalabra);
void agregarPalabra(char palabra[MAXIMA_LONGITUD_PALABRA]);
void imprimir(void);

void PalabrasArchivadasPorString(){
    FILE * fd;
    char buffer[sizeBufferNombre];
    char delimitador[] = ",;:. \n!\"'"; 
    fd = fopen("prueba.txt","r");
    if(fd == NULL){
        printf("Error!!\n\r");
        while(1);
    }else{
        while ( feof(fd) == 0){ // feof sirve para hallar el final al igual que EOF (fgetc lo hace caracter por caracter)
            fgets(buffer, sizeBufferNombre, fd);
        }
    }
    char *token = strtok(buffer, delimitador);
    while (token != NULL) {
    agregarPalabra(token);
    token = strtok(NULL, delimitador);
   }
    imprimir();
    fclose(fd);
}

void PalabrasArchivadasPorCaracter(){
    FILE * fd;
    char caracter;
    fd = fopen("prueba.txt","r");
    if(fd == NULL){
        printf("Error!!\n\r");
        while(1);
    }else{
        while ( (caracter = fgetc(fd)) != EOF){
            printf("%c",caracter);
        }
    }
    fclose(fd);
}

void agregar(DetalleDePalabra detalleDePalabra) {

  nodo *nuevoNodo = malloc(sizeof(nodo));
  // Le ponemos el dato
  nuevoNodo->detalleDePalabra = detalleDePalabra;
  // Y ahora el nuevo nodo es el superior, y su siguiente
  // es el que antes era superior
  nuevoNodo->siguiente = superior;
  superior = nuevoNodo;
}

void agregarPalabra(char palabra[MAXIMA_LONGITUD_PALABRA]) {
  nodo *temporal = superior;
  while (temporal != NULL) {
    // Comprobar si la encontramos
    int resultadoDeComparacion = strcasecmp(temporal->detalleDePalabra.palabra, palabra);
    // Si es 0, entonces sí
    if (resultadoDeComparacion == 0) {
      // Aumentar frecuencia y terminar ciclo y función
      temporal->detalleDePalabra.frecuencia++;
      return;
    }
    temporal = temporal->siguiente;
  }
  // Si no encontramos nada, agregamos una nueva
  DetalleDePalabra detalleDePalabra;
  strcpy(detalleDePalabra.palabra, palabra);
  detalleDePalabra.frecuencia = 1; // La primera vez es 1
  agregar(detalleDePalabra);
}

void imprimir(void) {
  // Un simple encabezado, no hay que confundirse
  char guiones[] = "--------------------";
  printf("+%s+%s+\n", guiones, guiones);
  printf("|%-20s|%-20s|\n", "PALABRA", "FRECUENCIA");
  printf("+%s+%s+\n", guiones, guiones);

  // A partir de aquí el código sí importa; simplemente recorremos la pila
  nodo *temporal = superior;
  while (temporal != NULL) {
    printf("|%-20s|%-20d|\n", temporal->detalleDePalabra.palabra, temporal->detalleDePalabra.frecuencia); 
    temporal = temporal->siguiente;
  }
}


#endif

int main(){
    PalabrasArchivadasPorString();
    return 0;
}



       